#!/usr/bin/env sh

command -v git >/dev/null 2>&1 || { apt-get install git >/dev/null 2>&1 && echo "= [dep] installing git..." }
command -v puppet >/dev/null 2>&1 || { apt-get install puppet >/dev/null 2>&1 && echo "= [dep] installing puppet..." }

echo "="

read -p "= bitbucket username : " username
read -s -p "= bitbucket password : " password

echo; echo "="

cd /etc/puppet/

if [ ! -d /etc/puppet/.git ]; then

  { git init } && { git remote add origin "https://$username:$password@bitbucket.org/ivoputzer/sparkle-puppet.git" }

fi

{ git add --all } && { git stash } && { git pull origin master }